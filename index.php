<?php
	echo "hello world";
	echo "end";

	/**
	* 
	*/
	class Person
	{
		private $name;
		private $surname;
		private $age;
		private $city;

		public function set_name($name){
			$this->name = $name;
		}

		public function set_surname($surname){
			$this->surname = $surname;
		}

		public function set_age($age){
			$this->age = $age;
		}

		public function set_city($city){
			$this->city = $city;
		}

		public function get_name(){
			return $this->name;
		}

		public function get_surname(){
			return $this->surname;
		}

		public function get_age(){
			return $this->age;
		}

		public function get_city(){
			return $this->city;
		}


	}
	class Cat
	{
		private $name;
		private $age;
		function __construct($name, $age)
		{
			$this->name = $name;
			$this->age = $age;
		}
	}
function printInfoPerson($person){
	echo $person->name." ".$person->surname;
}

function printInfoAgePerson($person){
	echo $person->age;
}